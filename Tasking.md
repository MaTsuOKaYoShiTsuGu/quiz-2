Story1：
AC1：
OK 1、新建Staff类和StaffRequest类，其中Staff类能保存姓名和id 
OK 2、新建Controller，实现使用StaffRequest类添加用户的POST请求逻辑，当请求正确时应返回201和Location
OK 3、当请求有误时，应当返回400

AC2：
OK 1、实现GET单个用户的方法，当使用GET请求获取已经存在的用户时应该返回200和对应的姓名以及id
OK 2、实现使用数据库对Staff类进行储存与读取
OK 3、当GET请求获取不存在的用户时应该返回404

AC3：
OK 1、实现GET所有用户的方法，当存在用户时返回所有用户的信息
2、当不存在用户时返回空数组

Story2：
AC1：
1、在Staff类中添加zoneId属性，并给staff表添加zoneId字段
2、在测试中的expect语句添加zoneId部分以让以前的测试通过
3、添加更新时区的PUT方法，当使用正确的请求时能成功更新时区信息并返回200
4、当请求有误的时候应该返回400

AC2：
1、当staff更改过时区时，查询staff得到的信息中应包含对应的时区
2、当staff未更改过时区时，查询staff得到当信息中时区对应的部分应该为null

Story3：
AC1：
1、添加获取时区表的GET方法，当使用正确的请求时能成功获取时区表信息并返回200


Story4：
AC1：
OK 1、添加Reservation类及其Repository接口
OK 2、添加预约的POST方法，当使用正确的请求时应该得到201
OK 3、添加对请求中body的验证，当使用错误的请求的时候应该得到400

AC2：
1、添加获取预约的GET方法，当使用正确的请求时能成功获取预约列表并返回200
