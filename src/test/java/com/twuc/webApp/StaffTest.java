package com.twuc.webApp;

import com.fasterxml.jackson.databind.JavaType;
import com.twuc.webApp.entity.ReservationRequest;
import com.twuc.webApp.entity.Staff;
import com.twuc.webApp.entity.StaffRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.zone.ZoneRulesProvider;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@AutoConfigureMockMvc
class StaffTest extends ApiTestBase{
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_save_name_and_id_in_staff(){
        StaffRequest staffRequest = new StaffRequest("firstName","lastName");
        Staff staff = new Staff(staffRequest.getFirstName(),staffRequest.getLastName());
        assertEquals("firstName",staff.getFirstName());
        assertEquals("lastName",staff.getLastName());
    }

    @Test
    void should_return_201_and_location_when_post_to_create_staff() throws Exception {
        StaffRequest staffRequest = new StaffRequest("Rob","Hall");
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(staffRequest)))
                .andExpect(MockMvcResultMatchers.header().string("Location","/api/staffs/1"))
                .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    void should_return_400_when_staff_name_contains_null() throws Exception {
        StaffRequest staffRequest = new StaffRequest("Rob",null);
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(staffRequest)))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_200_and_staff_info_when_get_exist_staff_id() throws Exception {
        addStaff("Rob","Hall");

        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$.firstName").value("Rob"));
    }

    @Test
    void should_return_404_when_staff_not_exist() throws Exception {
        mockMvc.perform(get("/api/staffs/2333"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }

    @Test
    void should_get_all_staffs() throws Exception {
        addStaff("Ai","Kayano");
        addStaff("Ayane","Sakura");

        String content = mockMvc.perform(get("/api/staffs"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andReturn().getResponse().getContentAsString();
        JavaType javaType = mapper.getTypeFactory().constructParametricType(List.class, Staff.class);
        List<Staff> staffs = mapper.readValue(content, javaType);
        assertEquals(2,staffs.size());
    }

    private void addStaff(String firstName,String lastName) throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new StaffRequest(firstName,lastName))));
    }

    @Test
    void should_get_null_array_when_no_staff_exist() throws Exception {
        mockMvc.perform(get("/api/staffs"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("[]"));
    }

    @Test
    void should_have_zone_id_property_in_staff_class() throws Exception {
        addStaff("Ai","Kayano");
        mockMvc.perform(get("/api/staffs/1"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$.firstName").value("Ai"));
//                .andExpect(MockMvcResultMatchers.content().string("{\"id\":1,\"firstName\":\"Ai\",\"lastName\":\"Kayano\",\"zoneId\":null}"));
    }

    @Test
    void should_return_200_and_update_zoneId_when_put_correct_zoneId() throws Exception {
        addStaff("Ai","Kayano");
        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"zoneId\":\"Asia/Chongqing\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_400_and_update_zoneId_when_put_wrong_zoneId() throws Exception {
        addStaff("Ai","Kayano");
        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"zoneId\":\"Asia/unknownAria\"}"))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }


    @Test
    void should_return_zoneId_list_when_get_zoneIds() throws Exception {
        Object[] ZoneIdList = ZoneRulesProvider.getAvailableZoneIds().toArray();
        Arrays.sort(ZoneIdList);
        mockMvc.perform(get("/api/timezones"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content()
                        .string(Arrays.toString(ZoneIdList)));
    }

    @Test
    void should_return_200_when_send_correct_reservation_info() throws Exception {
        addStaff("Ai","Kayano");
        ReservationRequest reservation = new ReservationRequest(
                "Sofia",
                "ThoughtWorks",
                ZoneId.of("Africa/Nairobi"),
                OffsetDateTime.parse("2019-08-20T11:46:00+03:00"),
                Duration.parse("PT1H"));

        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"zoneId\":\"Asia/Chongqing\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200));

        mockMvc.perform(post("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(reservation)))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.header().string("Location","/api/staffs/1/reservations"));
    }

    @Test
    void should_return_400_when_send_invalid_reservation() throws Exception {
        addStaff("Ai","Kayano");
        ReservationRequest reservation = new ReservationRequest(
                "Sofia",
                "ThoughtWorks",
                ZoneId.of("Africa/Nairobi"),
                OffsetDateTime.parse("2019-08-20T11:46:00+03:00"),
                null);
        mockMvc.perform(post("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(reservation)))
                .andExpect(MockMvcResultMatchers.status().is(400));
    }

    @Test
    void should_return_reservation_list_and_200_when_get_reservations() throws Exception {
        addStaff("Ai","Kayano");
        ReservationRequest reservation = new ReservationRequest(
                "Sofia",
                "ThoughtWorks",
                ZoneId.of("Africa/Nairobi"),
                OffsetDateTime.parse("2019-08-20T11:46:00+03:00"),
                Duration.parse("PT1H"));

        mockMvc.perform(put("/api/staffs/1/timezone")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"zoneId\":\"Asia/Chongqing\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200));

        mockMvc.perform(post("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(reservation)))
                .andExpect(MockMvcResultMatchers.status().is(201));

        mockMvc.perform(get("/api/staffs/1/reservations")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("[{\"userName\":\"Sofia\",\"companyName\":\"ThoughtWorks\",\"duration\":\"PT1H\",\"startTime\":{\"client\":{\"zoneId\":\"Africa/Nairobi\",\"startTime\":\"2019-08-20T11:46:00+03:00\"},\"staff\":{\"zoneId\":\"Asia/Chongqing\",\"startTime\":\"2019-08-20T16:46:00+08:00\"}}}]"));
    }
}
