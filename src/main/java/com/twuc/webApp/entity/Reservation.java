package com.twuc.webApp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.ZoneId;

@Entity
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long id;

    @Column
    private String userName;

    @Column
    private String companyName;

    @Column
    private Duration duration;

    @Embedded
    private ReservationStartTime startTime;

    public Reservation() {
    }

    public Reservation(String userName, String companyName, ZoneId clientZoneId, ZoneId staffZoneId, OffsetDateTime clientStartTime, Duration duration) {
        this.userName = userName;
        this.companyName = companyName;
        this.startTime = new ReservationStartTime(
                new StartTime(clientZoneId,OffsetDateTime.ofInstant(clientStartTime.toInstant(),clientZoneId)),
                new StartTime(staffZoneId,OffsetDateTime.ofInstant(clientStartTime.toInstant(),staffZoneId)));
        this.duration = duration;
    }

    public Long getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public ReservationStartTime getStartTime() {
        return startTime;
    }

    public Duration getDuration() {
        return duration;
    }
}
