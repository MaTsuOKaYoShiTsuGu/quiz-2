package com.twuc.webApp.entity;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.time.ZoneId;

public class ReservationRequest {

    @NotNull
    @Length(max = 128)
    private String userName;

    @NotNull
    @Length(max = 64)
    private String companyName;

    @NotNull
    private ZoneId zoneId;

    @NotNull
    private OffsetDateTime startTime;

    @NotNull
    private Duration duration;

    public String getUserName() {
        return userName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public OffsetDateTime getStartTime() {
        return startTime;
    }

    public Duration getDuration() {
        return duration;
    }

    public ReservationRequest(String userName, String companyName, ZoneId zoneId, OffsetDateTime startTime, Duration duration) {
        this.userName = userName;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }
}
