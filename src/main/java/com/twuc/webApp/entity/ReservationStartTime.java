package com.twuc.webApp.entity;

import javax.persistence.*;

@Embeddable
public class ReservationStartTime {

    @Embedded
    private StartTime client;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "zoneId",column = @Column(name = "staffZoneId")),
            @AttributeOverride(name = "startTime",column = @Column(name = "staffStartTime"))
    })
    private StartTime staff;

    public StartTime getStaff() {
        return staff;
    }

    public StartTime getClient() {
        return client;
    }

    public ReservationStartTime() {
    }

    public ReservationStartTime(StartTime client, StartTime staff) {
        this.client = client;
        this.staff = staff;
    }
}
