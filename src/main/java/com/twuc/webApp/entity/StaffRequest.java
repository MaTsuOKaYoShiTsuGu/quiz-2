package com.twuc.webApp.entity;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

public class StaffRequest {
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public StaffRequest(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @NotNull
    @Length(max = 64)
    private String firstName;

    @NotNull
    @Length(max = 64)
    private String lastName;
}
