package com.twuc.webApp.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.OffsetDateTime;
import java.time.ZoneId;

@Embeddable
public class StartTime {
    @Column
    private ZoneId zoneId;
    @Column
    private OffsetDateTime startTime;

    public ZoneId getZoneId() {
        return zoneId;
    }

    public OffsetDateTime getStartTime() {
        return startTime;
    }

    public StartTime() {
    }

    public StartTime(ZoneId zoneId, OffsetDateTime startTime) {
        this.zoneId = zoneId;
        this.startTime = startTime;
    }
}
