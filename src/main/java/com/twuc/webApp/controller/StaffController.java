package com.twuc.webApp.controller;

import com.twuc.webApp.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.ZoneId;
import java.time.zone.ZoneRulesProvider;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
public class StaffController {

    @Autowired
    private StaffRepository staffRepository;

    @Autowired
    private ReservationRepository reservationRepository;

    @PostMapping("/api/staffs")
    ResponseEntity createStaff(@RequestBody @Valid StaffRequest staffRequest) {
        Staff staff = new Staff(staffRequest.getFirstName(), staffRequest.getLastName());
        staffRepository.save(staff);
        return ResponseEntity.status(201)
                .header("Location", "/api/staffs/" + staff.getId())
                .build();
    }

    @GetMapping("/api/staffs/{staffId}")
    ResponseEntity getStaff(@PathVariable Long staffId) {
        Optional<Staff> findResult = staffRepository.findById(staffId);
        if (findResult.isPresent()) {
            return ResponseEntity.status(200)
                    .body(findResult.get());
        } else
            return ResponseEntity.status(404).build();
    }

    @GetMapping("/api/staffs")
    ResponseEntity getStaffs() {
        List<Staff> staffs = staffRepository.findAll();
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON)
                .body(staffs);
    }

    @PutMapping("/api/staffs/{staffId}/timezone")
    ResponseEntity updateZoneId(@PathVariable Long staffId,@RequestBody String zoneId){
        Optional<Staff> findResult = staffRepository.findById(staffId);
        String zoneIdValue = getZoneIdValue(zoneId);
        if (findResult.isPresent()) {
            Staff staff = findResult.get();
            if(!verifyZoneId(zoneIdValue)){
                return ResponseEntity.status(400).build();
            }
            staff.setZoneId(ZoneId.of(zoneIdValue));
            staffRepository.save(staff);
            return ResponseEntity.status(200)
                    .body(staff);
        } else
            return ResponseEntity.status(404).build();
    }

    private String getZoneIdValue(String zoneId){
        return zoneId.substring(zoneId.indexOf(":")+2,zoneId.indexOf("}")-1);
    }

    private boolean verifyZoneId(String zoneIdValue){
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        return availableZoneIds.contains(zoneIdValue);
    }

    @GetMapping("/api/timezones")
    ResponseEntity getZoneIdList(){
        Object[] ZoneIdList = ZoneRulesProvider.getAvailableZoneIds().toArray();
        Arrays.sort(ZoneIdList);
        return ResponseEntity.status(200).body(Arrays.toString(ZoneIdList));
    }

    @PostMapping("/api/staffs/{staffId}/reservations")
    ResponseEntity createReservation(@PathVariable Long staffId, @RequestBody @Valid ReservationRequest reservationRequest){
        Optional<Staff> findResult = staffRepository.findById(staffId);

        if(!findResult.isPresent()){
            return ResponseEntity.status(404).build();
        }

        Staff foundStaff = findResult.get();
        Reservation reservation = new Reservation(
                reservationRequest.getUserName(),
                reservationRequest.getCompanyName(),
                reservationRequest.getZoneId(),
                foundStaff.getZoneId(),
                reservationRequest.getStartTime(),
                reservationRequest.getDuration()
        );
        foundStaff.getReservations().add(reservation);
        staffRepository.flush();

        return ResponseEntity.status(201)
                .header("Location",String.format("/api/staffs/%d/reservations",reservation.getId()))
                .build();
    }

    @GetMapping("/api/staffs/{staffId}/reservations")
    ResponseEntity getReservations(@PathVariable Long staffId){
        Optional<Staff> findResult = staffRepository.findById(staffId);

        if(!findResult.isPresent()){
            return ResponseEntity.status(404).build();
        }

        Staff foundStaff = findResult.get();

        return ResponseEntity.status(200).body(foundStaff.getReservations());
    }
}
